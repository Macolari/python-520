from pymongo import MongoClient

client = MongoClient()
db = client['python']

usuarios = {'nome' : 'Mongo', 'email' : 'mongo@monguinho.com'}
db.usuarios.insert(usuarios)

for x in db.usuarios.find({}, {'nome' : {'$exists' : True}}):
    print(x)