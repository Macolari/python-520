from conta import Conta, Poupanca

c1 = Conta('Victor', 2000)
c2 = Poupanca('Giovanna', 2000)

print(c2.saldo, c2.titular)
c2.render_juros()
print(c2.saldo)