from time import sleep
class Dog():
    def __init__(self, nome, raca, idade=1):
        self.nome = nome
        self.raca = raca
        self.idade = idade
        self.energia = 3

    def latir(self):
        print('auauau')
    def andar(self):
        if self.energia > 0:
            self.energia -= 1
            print('andando...')
            return True
        else:
            self.dormir()
            return False
    def dormir(self):
        self.energia = 3
        for x in range(10):
            if x % 2 == 0:
                print('zZz..', end='\r')
            else:
                print('ZzZ...', end='\r')
            sleep(1)

    
dog = Dog('Nina', 'Pastor Alemao', 2)
dog2 = Dog('Rex', 'Lhasa-Apso')

print(dog.nome, dog.energia, sep='\n')
dog.andar()
dog.andar()
dog.andar()
dog.andar()


