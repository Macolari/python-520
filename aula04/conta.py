class Conta():
    def __init__(self, titular, saldo=0):
        self.titular = titular
        self.saldo = saldo

    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor * 0.05
            print("Saque realizado com sucesso!")
            return True 
        else:
            print("Saldo insuficiente, operacao cancelada!")
            return False 
    def depositar(self, valor):
        self.saldo += valor
    def transferir(self, valor, conta):
        try:
            if not self.sacar(valor):
                raise ValueError('Falha na transferencia!')
            else:
                try:
                    conta.depositar(valor)
                except AttributeError:
                    print('Conta invalida!')
                    self.depositar(valor)
        except ValueError as e:
            print(e)

'''
c1 = Conta('Victor Azevedo', 1000)
c2 = Conta('Giovanna Scarpelli', 2500)
c3 = 'string'

print('Seu saldo é de: R${:.2f}'.format(c1.saldo))
c1.transferir(100, c3)
print('Seu saldo é de: R${:.2f}'.format(c1.saldo))
'''

class Poupanca(Conta):
    def render_juros(self):
        self.saldo += self.saldo * 0.005
    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor 
            print("Saque realizado com sucesso!")
            return True 
        else:
            print("Saldo insuficiente, operacao cancelada!")
            return False 
c1 = Conta('Victor Azevedo', 2000)
c2 = Poupanca('Giovanna Scarpelli', 2500)

print(c2.saldo, c2.titular)
c2.render_juros()
print(c2.saldo)




