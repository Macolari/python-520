#!/usr/bin/python3

qtd_alunos = int(input("Quantidade de alunos: "))
qtd_notas = int(input("Quantidade de notas: "))
alunos = []
for y in range(qtd_alunos):
    nome = input("Nome do aluno: ")
    soma = 0
    for x in range(qtd_notas):
        nota = float(input("Digite a nota{}: ".format(x+1)))
        if nota > 10:
            qtd_notas -= 1
            continue
        soma += nota
    media = soma / qtd_notas
    if media >= 7:
        result = 'APROVADO'
    elif media > 3:
        result = 'RECUPERACAO'
    else:
        result = 'REPROVADO'
    aluno = {
        'nome': nome,
        'media': media,
        'status': result
    }
    alunos.append(aluno)

print(alunos)
